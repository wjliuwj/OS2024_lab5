#include "asm_utils.h"
#include "interrupt.h"
#include "stdio.h"
#include "program.h"
#include "thread.h"

// 屏幕IO处理器
STDIO stdio;
// 中断管理器
InterruptManager interruptManager;
// 程序管理器
ProgramManager programManager;

void third_thread(void *arg) {
    //printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    printf("NOW IN THIRD_THREAD FUNCTION!\n");
    while(1) {

    }
}
void second_thread(void *arg) {
    //printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    printf("NOW IN SECOND_THREAD FUNCTION!\n");
    asm_halt();
}
void first_thread(void *arg)
{
    // 第1个线程不可以返回
    printf("NOW IN FIRST_THREAD FUNCTION!\n");
    if (!programManager.running->pid)
    {   
        printf("second_thread coming!\n");
        programManager.executeThread(second_thread, nullptr, "second thread", 2, 3);
        printf("third_thread coming!\n");
        programManager.executeThread(third_thread, nullptr, "third thread", 1, 2);
    }
    asm_halt();
}

extern "C" void setup_kernel()
{

    // 中断管理器
    interruptManager.initialize();
    interruptManager.enableTimeInterrupt();
    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);

    // 输出管理器
    stdio.initialize();

    // 进程/线程管理器
    programManager.initialize();

    // 手动创建第一个线程
    printf("first_thread coming!\n");
    int pid = programManager.executeThread(first_thread, nullptr, "first thread", 1, 6);
    if (pid == -1)
    {
        printf("can not execute thread\n");
        asm_halt();
    }
    // 由于当前系统中没有线程，因此我们无法通过在时钟中断调度的方式将第一个线程换上处理器执行。
    // 因此我们的做法是找出第一个线程的PCB，然后手动执行类似schedule的过程，
    // 最后执行的asm_switch_thread会强制将第一个线程换上处理器执行。
    ListItem *item = programManager.readyPrograms.front();
    PCB *firstThread = ListItem2PCB(item, tagInGeneralList);
    firstThread->status = RUNNING;
    firstThread->ticks = firstThread->ticksTotal; //相当于分配的时间就是需要的执行总时间
    programManager.readyPrograms.pop_front();
    programManager.running = firstThread;
    asm_switch_thread(0, firstThread);

    asm_halt();
}
