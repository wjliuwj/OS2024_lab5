#include "program.h"
#include "stdlib.h"
#include "interrupt.h"
#include "asm_utils.h"
#include "stdio.h"
#include "thread.h"
#include "os_modules.h"

const int PCB_SIZE = 4096;                   // PCB的大小，4KB。
char PCB_SET[PCB_SIZE * MAX_PROGRAM_AMOUNT]; // 存放PCB的数组，预留了MAX_PROGRAM_AMOUNT个PCB的大小空间。
bool PCB_SET_STATUS[MAX_PROGRAM_AMOUNT];     // PCB的分配状态，true表示已经分配，false表示未分配。

ProgramManager::ProgramManager()
{
    initialize();
}

void ProgramManager::initialize()
{
    allPrograms.initialize();
    readyPrograms.initialize();
    running = nullptr;

    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        PCB_SET_STATUS[i] = false;
    }
}

int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority, int time)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticksTotal = time;
    thread->ticks = 3;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    // 初始化线程的栈。我们将栈放置在PCB中，而线程的栈是从PCB的顶部开始向下增长的，
    // 所以不会与位于PCB低地址的name和pid等变量冲突。线程栈的初始地址是PCB的起始地址加上PCB_SIZE
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    // 4个为0的值是要放到ebp，ebx，edi，esi中的
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    // thread->stack[4]是线程执行的函数的起始地址。
    thread->stack[4] = (int)function;
    // thread->stack[5]是线程的返回地址，所有的线程执行完毕后都会返回到这个地址。
    thread->stack[5] = (int)program_exit;
    // thread->stack[6]是线程的参数的地址。
    thread->stack[6] = (int)parameter;

    // 创建完线程的PCB后，我们将其放入到allPrograms和readyPrograms中，
    // 等待时钟中断来的时候，这个新创建的线程就可以被调度上处理器
    allPrograms.push_back(&(thread->tagInAllList));
    if(readyPrograms.empty()){
        readyPrograms.push_back(&(thread->tagInGeneralList));
    }else{
        ListItem *item = programManager.readyPrograms.front();
        PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
        if(thread->ticksTotal <= thisThread->ticksTotal){
            readyPrograms.push_front(&(thread->tagInGeneralList));
        }else{
            int pos = 1;
            ListItem *item = programManager.readyPrograms.at(pos);
            PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            while(thread->ticksTotal > thisThread->ticksTotal){
                if(pos == programManager.readyPrograms.size() - 1){
                    pos ++;
                    break;
                }
                pos ++;
                ListItem *item = programManager.readyPrograms.at(pos);
                PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            }
            programManager.readyPrograms.insert(pos, &(thread->tagInGeneralList));
        }
    }
    

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    printf("pid %d name %s priority %d totalTime %d CREATEDandREADY!\n",thread->pid,thread->name,thread->priority,thread->ticksTotal);
    //printf("Its addr: %d stack addr: %d\n",(int)thread, (int)thread->stack);

    return thread->pid;
}

void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // readyPrograms为空则说明只有1个进程
    if (readyPrograms.size() == 0 && running->ticksTotal == 0)
    {
        interruptManager.setInterruptStatus(status);
        printf("---------------FINISH----------------");
        while(1){}
        return;
    }
    

    // running的变ready，且放到ready队列队尾
    if (running->status == ProgramStatus::RUNNING && running->ticksTotal)
    {
        running->status = ProgramStatus::READY;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::DEAD;
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }
    // 去就绪队列的第一个线程作为下一个执行的线程。
    // 就绪队列的第一个元素是ListItem *类型的，我们需要将其转换为PCB。
    // 注意到放入就绪队列readyPrograms的是每一个PCB的&tagInGeneralList，
    // 而tagInGeneralList在PCB中的偏移地址是固定的。
    // 也就是说，我们将item的值减去tagInGeneralList在PCB中的偏移地址就能够得到PCB的起始地址。
    // #define ListItem2PCB(ADDRESS, LIST_ITEM) ((PCB *)((int)(ADDRESS) - (int)&((PCB *)0)->LIST_ITEM))
    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    next->ticks = 3; //给下一个调度上来的进程分配时间片
    running = next;
    readyPrograms.pop_front();

    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}

void program_exit()
{
    PCB *thread = programManager.running;
    thread->status = ProgramStatus::DEAD;

    // 规定第一个线程(pid=0)的不可以返回
    if (thread->pid)
    {
        programManager.schedule();
    }
    else
    {
        interruptManager.disableInterrupt();
        printf("halt\n");
        asm_halt();
    }
}

PCB *ProgramManager::allocatePCB()
{
    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        if (!PCB_SET_STATUS[i])
        {
            PCB_SET_STATUS[i] = true;
            return (PCB *)((int)PCB_SET + PCB_SIZE * i);
        }
    }

    return nullptr;
}

void ProgramManager::releasePCB(PCB *program)
{
    int index = ((int)program - (int)PCB_SET) / PCB_SIZE;
    PCB_SET_STATUS[index] = false;
}