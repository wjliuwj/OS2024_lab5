#include "asm_utils.h"
#include "interrupt.h"
#include "stdio.h"
#include "program.h"
#include "thread.h"

// 屏幕IO处理器
STDIO stdio;
// 中断管理器
InterruptManager interruptManager;
// 程序管理器
ProgramManager programManager;

void third_thread(void *arg) {
    printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    while(1) {

    }
    
}
void second_thread(void *arg) {
    printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    asm_halt();
}
void first_thread(void *arg)
{
    // 第1个线程不可以返回
    printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    if (!programManager.running->pid)
    {
        programManager.executeThread(second_thread, nullptr, "second thread", 2);
        programManager.executeThread(third_thread, nullptr, "third thread", 3);
    }
    asm_halt();
}

extern "C" void setup_kernel()
{

    // 中断管理器
    interruptManager.initialize();
    interruptManager.enableTimeInterrupt();
    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);

    // 输出管理器
    stdio.initialize();

    // 进程/线程管理器
    programManager.initialize();

    // 手动创建第一个线程
    int pid = programManager.executeThread(first_thread, nullptr, "first thread", 1);
    if (pid == -1)
    {
        printf("can not execute thread\n");
        asm_halt();
    }
    // 由于当前系统中没有线程，因此我们无法通过在时钟中断调度的方式将第一个线程换上处理器执行。
    // 因此我们的做法是找出第一个线程的PCB，然后手动执行类似schedule的过程，
    // 最后执行的asm_switch_thread会强制将第一个线程换上处理器执行。
    ListItem *item = programManager.readyPrograms.front();
    PCB *firstThread = ListItem2PCB(item, tagInGeneralList);
    firstThread->status = RUNNING;
    programManager.readyPrograms.pop_front();
    programManager.running = firstThread;
    asm_switch_thread(0, firstThread);

    asm_halt();
}
