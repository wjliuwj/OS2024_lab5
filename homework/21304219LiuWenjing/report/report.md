<font size =6>**操作系统原理 实验五**</font>

## 个人信息

【院系】计算机学院

【专业】计算机科学与技术

【学号】21304219

【姓名】刘文婧

## 实验题目

内核线程

## 实验目的

1. 学习C语言的可变参数机制的实现方法，实现可变参数机制，以及实现一个较为简单的printf函数。
2. 实现内核线程的实现。
3. 重点理解`asm_switch_thread`是如何实现线程切换的，体会操作系统实现并发执行的原理。
4. 实现基于时钟中断的时间片轮转(RR)调度算法，并实现一个线程调度算法。

## 实验要求

1. 实现了可变参数机制及printf函数。
2. 自行实现PCB，实现线程。
3. 了解线程调度，并实现一个调度算法。
4. 撰写实验报告。

## 实验方案

### 硬件或虚拟机配置方法
* WSL2(Ubuntu)

### 软件工具与作用
* git: 代码管理
* VSCode: 连接linux系统
* Ubuntu22.04

### 方案的思想、相关原理、程序流程、算法和数据结构
### C语言的可变参数机制
* 为了引用可变参数列表中的参数，我们需要用到<stdarg.h>头文件定义的一个变量类型va_list和三个宏va_start，va_arg，va_end，这三个宏用于获取可变参数列表中的参数
  - va_list：定义一个指向可变参数列表的指针。
  - void va_start(va_list ap, last_arg)：初始化可变参数列表指针ap，使其指向可变参数列表的起始位置，即函数的固定参数列表的最后一个参数last_arg的后面第一个参数。
  - type va_arg(va_list ap, type)：以类型type返回可变参数，并使ap指向下一个参数。
  - void va_end(va_list ap)：	清零ap。        
* 从本质上来说，parameter就是指向函数调用栈的一个指针，类似esp、ebp，va_arg按照指定的类型来返回parameter指向的内容。注意，在va_arg返回后，parameter会指向下一个参数，无需我们手动调整。

* 访问完可变参数后，我们需要使用va_end对parameter进行清零，这是防止后面再使用va_arg和parameter来引用栈中的内容，从而导致错误。

### 可变参数机制的实现
* 在保护模式下，栈中的push和pop的变量都是32位的，也就是4个字节。无论是char、short还是int，这些变量在栈中都是以4字节对齐的形式保存的。4字节对齐的意思是找到第一个不小于变量的长度且为4的倍数的整数。例如，char放在栈中时，虽然char是1个字节，但是栈使用4个字节来保存它。而ap是指向栈的，因此ap需要4个字节对齐，也就是ap的值需要是4的倍数。
* 所以需要定义一个_INTSIZEOF(n)，从而返回n的大小进行4字节对齐的结果
* 于是上面的宏具体实现如下
```c
typedef char *va_list;
#define _INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))
#define va_start(ap, v) (ap = (va_list)&v + _INTSIZEOF(v))
#define va_arg(ap, type) (*(type *)((ap += _INTSIZEOF(type)) - _INTSIZEOF(type)))
#define va_end(ap) (ap = (va_list)0)
```

### 实现printf
* 首先，实现能处理`\n`的函数（\n的换行效果是我们人为规定的，换行的实现需要我们把光标放到下一行的起始位置，如果光标超过了屏幕的表示范围，则需要滚屏）
* 然后，实现fmt的解析：`printf`首先找到`fmt`中的形如`%c,%d,%x,%s`对应的参数，然后用这些参数具体的值来替换%c,%d,%x,%s等，得到一个新的**格式化输出字符串**，这个过程称为fmt的解析。最后，printf将这个新的格式化输出字符串即可。然而，这个字符串可能非常大，会超过函数调用栈的大小。实际上，我们会**定义一个缓冲区，然后对fmt进行逐字符地解析**，将**结果逐字符的放到缓冲区中**。放入一个字符后，我们会检查缓冲区，如果缓冲区已满，则将其输出，然后清空缓冲区，否则不做处理
* 代码参考教程，见src/Assignment1

### 用户线程和内核线程
> 用户线程指线程**只由用户进程来实现，操作系统中无线程机制。在用户空间中实现线程的好处是可移植性强**，由于是用户级的实现，所以在不支持线程的操作系统上也可以写出完美支持线程的用户程序。但是，用户线程存在以下缺点。

* 若进程中的某个线程出现了阻塞，操作系统不知道进程中存在线程，因此会将整个进程挂起，导致**进程中的全部线程都无法运行**。
> 所以需要内核线程：   
* 相比在用户空间中实现线程，内核提供的线程相当于**让进程多占了处理器资源**，比如系统中运行有进程 A和一传统型进程B（无内核线程，用户线程机制）。假设进程A中显式地创建了3个线程，这样一来，进程A加上主线程便有了4个线程，加上进程B，在内核调度器眼中便有了5个独立的执行流。尽管其中4个线程都属于进程A，但对调度器来说这4个线程和进程一样被调度，因此**调度器调度完一圈后，进程A使用了 80%的处理器资源，这才是真正的提速**。    
* 另一方面的优点是当进程中的某一线程阻塞后，由于线程是由内核空间实现的，操作系统认识线程，所以**就只会阻塞这一个线程，此线程所在进程内的其他线程将不受影响，这又相当于提速**了。
* 用户进程需要通过系统调用陷入内核，这多少增加了一些现场保护的栈操作，这还是会消耗一些处理器时间，但和上面的大幅度提速相比，这显然是微不足道的。        
  
### 进程和线程的实现顺序
* 计算机**先有进程，然后再有线程**
* 进程是有自己的地址空间的，而线程是共享父进程的地址空间的
* 线程实际上执行的是某一个函数，这个函数使用的是**父进程的地址空间**。注意到我们创建的线程是内核线程，并且我们现在运行的环境只有内核态。因此，线程的创建和使用并不会涉及到内存管理的内容。

### 线程的描述
* 我们创建的线程的状态有5个，分别是创建态、运行态、就绪态、阻塞态和终止态。我们使用一个**枚举类型**ProgramStatus来描述线程的5个状态
```c
enum ProgramStatus
{
    CREATED,
    RUNNING,
    READY,
    BLOCKED,
    DEAD
};
```
* 线程的组成部分:线程各自的**栈，状态，优先级，运行时间，线程负责运行的函数，函数的参数**等，这些组成部分被集中保存在一个结构中——PCB(Process Control Block)
```c
struct PCB
{
    int *stack;                      // 栈指针，用于调度时保存esp
    char name[MAX_PROGRAM_NAME + 1]; // 线程名
    enum ProgramStatus status;       // 线程的状态
    int priority;                    // 线程优先级
    int pid;                         // 线程pid
    int ticks;                       // 线程时间片总时间
    int ticksPassedBy;               // 线程已执行时间
    ListItem tagInGeneralList;       // 线程队列标识
    ListItem tagInAllList;           // 线程队列标识
};
```
![alt text](img/image2.png)
![alt text](img/image3.png)

### PCB的分配
> 在创建线程之前，我们需要向内存申请一个PCB。我们将一个PCB的大小设置为**4096个字节，也就是一个页的大小**。
```c
// PCB的大小，4KB。
const int PCB_SIZE = 4096;         
// 存放PCB的数组，预留了MAX_PROGRAM_AMOUNT个PCB的大小空间。
char PCB_SET[PCB_SIZE * MAX_PROGRAM_AMOUNT]; 
// PCB的分配状态，true表示已经分配，false表示未分配。
bool PCB_SET_STATUS[MAX_PROGRAM_AMOUNT];     
```
* 分配PCB:
* allocatePCB会去检查`PCB_SET`中每一个PCB的状态，如果找到一个未被分配的PCB，则**返回这个PCB的起始地址**。注意到`PCB_SET`中的PCB是连续存放的，对于第i个PCB，`PCB_SET`的首地址加上i×$PCBSIZE$
就是第i个PCB的起始地址。PCB的状态保存在`PCB_SET_STATUS`中，并且`PCB_SET_STATUS`的每一项会在ProgramManager总被初始化为false，表示所有的PCB都未被分配。被分配的PCB用true来标识。
* 如果PCB_SET_STATUS的所有元素都是true，表示所有的PCB都已经被分配，此时应该返回nullptr，表示PCB分配失败。
```c
PCB *ProgramManager::allocatePCB()
{
    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        if (!PCB_SET_STATUS[i])
        {
            PCB_SET_STATUS[i] = true;
            return (PCB *)((int)PCB_SET + PCB_SIZE * i);
        }
    }

    return nullptr;
}
```
* 释放PCB:
* releasePCB接受一个PCB指针program，然后计算出program指向的PCB在PCB_SET中的位置，然后将PCB_SET_STATUS中的对应位置设置false即可
```c
void ProgramManager::releasePCB(PCB *program)
{
    int index = ((int)program - (int)PCB_SET) / PCB_SIZE;
    PCB_SET_STATUS[index] = false;
}
```
### 线程的创建
* 主要是由函数`executeThread`实现，如下：
```c
int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticks = priority * 10;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    // 初始化线程的栈。我们将栈放置在PCB中，而线程的栈是从PCB的顶部开始向下增长的，
    // 所以不会与位于PCB低地址的name和pid等变量冲突。线程栈的初始地址是PCB的起始地址加上PCB_SIZE
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    // 4个为0的值是要放到ebp，ebx，edi，esi中的
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    // thread->stack[4]是线程执行的函数的起始地址。
    thread->stack[4] = (int)function;
    // thread->stack[5]是线程的返回地址，所有的线程执行完毕后都会返回到这个地址。
    thread->stack[5] = (int)program_exit;
    // thread->stack[6]是线程的参数的地址。
    thread->stack[6] = (int)parameter;

    // 创建完线程的PCB后，我们将其放入到allPrograms和readyPrograms中，
    // 等待时钟中断来的时候，这个新创建的线程就可以被调度上处理器
    allPrograms.push_back(&(thread->tagInAllList));
    readyPrograms.push_back(&(thread->tagInGeneralList));

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    return thread->pid;
}
```
* ProgramManager类如下:
```c
class ProgramManager
{
public:
    List allPrograms;   // 所有状态的线程/进程的队列
    List readyPrograms; // 处于ready(就绪态)的线程/进程的队列
    PCB *running;       // 当前执行的线程
public:
    ProgramManager();
    void initialize();

    // 创建一个线程并放入就绪队列
    // function：线程执行的函数
    // parameter：指向函数的参数的指针
    // name：线程的名称
    // priority：线程的优先级
    // 成功，返回pid；失败，返回-1
    int executeThread(ThreadFunction function, void *parameter, const char *name, int priority);

    // 分配一个PCB
    PCB *allocatePCB();
    // 归还一个PCB
    // program：待释放的PCB
    void releasePCB(PCB *program);
};
```

### 线程的调度
* 修改时钟中断处理函数：
```c
extern "C" void c_time_interrupt_handler()
{
    PCB *cur = programManager.running;

    if (cur->ticks)
    {
        --cur->ticks;
        ++cur->ticksPassedBy;
    }
    else
    {
        //没有时间片了，就调度
        programManager.schedule();
    }
}
```
* 然后不妨以Round_Robin调度算法为例，实现schedule如下：
```c
void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        return;
    }

    // running的变ready，且放到ready队列队尾
    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::READY;
        running->ticks = running->priority * 10;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }
    // 去就绪队列的第一个线程作为下一个执行的线程。
    // 就绪队列的第一个元素是ListItem *类型的，我们需要将其转换为PCB。
    // 注意到放入就绪队列readyPrograms的是每一个PCB的&tagInGeneralList，
    // 而tagInGeneralList在PCB中的偏移地址是固定的。
    // 也就是说，我们将item的值减去tagInGeneralList在PCB中的偏移地址就能够得到PCB的起始地址。
    // #define ListItem2PCB(ADDRESS, LIST_ITEM) ((PCB *)((int)(ADDRESS) - (int)&((PCB *)0)->LIST_ITEM))
    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    running = next;
    readyPrograms.pop_front();

    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}
```
* 而上面用来进行进程切换的汇编函数如下：
```x86asm
asm_switch_thread:
    push ebp
    push ebx
    push edi
    push esi
    
    ; 向cur->stack中写入esp
    mov eax, [esp + 5 * 4]
    mov [eax], esp ; 保存当前栈指针到PCB中，以便日后恢复

    ; 将next->stack的值写入到esp中，从而完成线程栈的切换
    mov eax, [esp + 6 * 4]
    mov esp, [eax] ; 此时栈已经从cur栈切换到next栈

    pop esi
    pop edi
    pop ebx
    pop ebp

    sti
    ret
```

包括：硬件或虚拟机配置方法、软件工具与作用、方案的思想、相关原理、程序流程、算法和数据结构、程序关键模块，结合代码与程序中的位置位置进行解释。不得抄袭，否则按作弊处理。

## 实验过程
### Assignment 1 printf的实现
* 根据教程给出的printf（代码见src/Assignment1）
* 运行结果如下：
  ![alt text](img/image1.png)
### Assignment 2 线程的实现
* 像实验教程一样设计好PCB，其余代码见src/Assignment2：
```c
struct PCB
{
    int *stack;                      // 栈指针，用于调度时保存esp
    char name[MAX_PROGRAM_NAME + 1]; // 线程名
    enum ProgramStatus status;       // 线程的状态
    int priority;                    // 线程优先级
    int pid;                         // 线程pid
    int ticks;                       // 线程时间片总时间
    int ticksPassedBy;               // 线程已执行时间
    ListItem tagInGeneralList;       // 线程队列标识
    ListItem tagInAllList;           // 线程队列标识
};
```
* 然后稍微更改`first_thread`函数，使得创建完第1个线程之后，继续创建第2、第3个线程，得到如下结果：
![alt text](img/image4.png)
### Assignment 3 线程调度切换的秘密
* 再次回顾一下主要的几个函数的作用：
  - `executeThread`创建线程，且加入到Programs队尾
  - `c_time_interrupt_handler()`时钟中断发生时，进行相关的处理（调度、ticks--等工作）
  - `schedule`线程调度函数
* 可以先修改时钟中断处理函数`c_time_interrupt_handler()`，多输出优先级、剩余ticks（时间片）等，然后修改一下`first_thread`,`second_thread`,`third_thread`，输出一些信息进行初步的观察：(根据实验教程写的RR调度算法，这里改了priority之后，其实相当于会把分给每个线程的时间片都给改变了)
```c
// 中断处理函数
extern "C" void c_time_interrupt_handler()
{
    PCB *cur = programManager.running;

    if (cur->ticks)
    {
        --cur->ticks;
        ++cur->ticksPassedBy;
        printf("pid %d named %s with priority %d ticks left: %d\n",cur->pid, cur->name, cur->priority, cur->ticks);
        printf("its stack address: %d\n", cur->stack);
    }
    else
    {
        printf("Calling schedule!\n");
        programManager.schedule();
    }
}
```
* Note:要加上`asm_halt()`或者`while(1)`，从而有进程一直在"运行"的效果，用完时间片才被调度下去
```c
void third_thread(void *arg) {
    printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    while(1) {

    }
}
void second_thread(void *arg) {
    printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    asm_halt();
}
void first_thread(void *arg)
{
    // 第1个线程不可以返回
    printf("pid %d with priority %d, named \"%s\": Hello World!\n", programManager.running->pid, programManager.running->priority, programManager.running->name);
    if (!programManager.running->pid)
    {
        programManager.executeThread(second_thread, nullptr, "second thread", 2);
        programManager.executeThread(third_thread, nullptr, "third thread", 3);
    }
    asm_halt();
}
```
* 于是可以看到：
  - `first_thread`到`second_thread`
  ![alt text](img/image6.png)
  - `second_thread`到`third_thread`
  ![alt text](img/image7.png)
  - `third_thread`到`first_thread`
  ![alt text](img/image5.png)

* 然后使用GDB跟踪：
> 使用gdb跟踪c_time_interrupt_handler、asm_switch_thread等函数，观察线程切换前后栈、寄存器、PC等变化，结合gdb、材料中“线程的调度”的内容来跟踪并说明下面两个过程。
> 1. 一个新创建的线程是如何被调度然后开始执行的。
> 2. 一个正在执行的线程是如何被中断然后被换下处理器的，以及换上处理机后又是如何从被中断点开始执行的。
* 实际上，`file ../build/kernel.o`这条用来加载符号表的命令写好在了gdbinit里，但要记得安装gnome-terminal：`sudo apt-get install gnome-terminal`
* 否则就只能手动输入`file ../build/kernel.o`命令，如下：
  ![alt text](img/image8.png)
* 安装好gnome-terminal后，`make debug`，GDB terminal就会自动弹出来了(gdbinit文件里写好了的)
* 使用GDB观察栈、寄存器变化如下：
* 没有把进入asm_switch_thread前，寄存器值如下：
  ![alt text](img/image17.png)
* 进入后
 ![alt text](img/image18.png)
* 更新esp从而能调度first_thread后：
  ![alt text](img/image24.png)
* 从first_thread切换到second_thread:
  没有更新esp前：
  ![alt text](img/image19.png)
  更新esp后：
  ![alt text](img/image20.png)
* 从second_thread到third_thread:
  没有更新esp前：
  ![alt text](img/image21.png)
  更新esp后：
  ![alt text](img/image22.png)
* 再切换回first_thread:
  ![alt text](img/image23.png)
* 从上面这些过程可以看出来，esp更新前后都大概有一页的空间大小的差别，这就是我们前面使用`PCB *thread = allocatePCB();`给每个线程分配的PCB的大小，而且每次esp上保存的值，就是从高地址往低地址增长的每一页上`PCB::stack`的地址位置
#### 一个新创建的线程是如何被调度然后开始执行的：  
* 根据schedule函数，从ready队列中取出要执行的下一个线程，然后利用`asm_switch_thread`函数进行线程切换，且实际上，**线程的切换就是线程栈的切换**，而**线程栈的切换就是把线程的栈指针放到esp中**
#### 一个正在执行的线程是如何被中断然后被换下处理器的，以及换上处理机后又是如何从被中断点开始执行的：    
* `c_time_interrupt_handler()`会在每次中断发生时让ticks--，ticks减到0时（或者是当前线程已经被执行完毕）就会去调用`schedule`函数，从而把当前进程换下来，并且把当前执行状态（这里线程保存的就是栈指针被存放的寄存器esp）等保存，即**保存esp的值到线程的PCB::statck中**。这样下次该线程被换上来时，就能从PCB恢复当前线程上次执行到的状态，即**把线程对应的PCB::stack写到esp中**，从而能从之前的中断点开始执行
### Assignment 4 调度算法的实现
* 为了更好地展示调度算法，在PCB中多加`totalticks`属性，3个ticks相关属性区分如下：
  - `ticksTotal`表示线程所需总时间
  - `ticks`是线程每次被调度上来后被分到的时间片
  - `ticksPassedBy`是线程已经执行完的总时间
* 然后稍微更改线程创建函数`executeThread`等相关部分
### FCFS
* 那么就对调度函数`ProgramManager::schedule()`作出修改：
```c
//FCFS
void ProgramManager::schedule()
{   
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        printf("----------FINISH------------");
        while(1){}
    }
    
    // FCFS运行完的线程就不会再被调度了
    if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::DEAD;
        //readyPrograms.push_back(&(running->tagInGeneralList));
    }
    if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }
    // 就绪队列的第一个线程作为下一个执行的线程。
    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    next->ticks = next->ticksTotal;//分给的ticks就是运行所需的总时间
    running = next;
    readyPrograms.pop_front();

    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}
```
* 得到如下结果（注意到，由于线程运行的同时，时钟中断总是在发生，所以会有下面2种可能的输出结果，他们在开头的语句顺序稍有区别，但都是FCFS方式调度的）：
  ![alt text](img/image9.png)
  ![alt text](img/image10.png)
### RR(在教程基础上稍微进行了修改)
* 中断处理函数如下：
```c
// 中断处理函数
extern "C" void c_time_interrupt_handler()
{
    PCB *cur = programManager.running;

    if (cur->ticks && cur->ticksTotal)
    {
        --cur->ticks;
        --cur->ticksTotal;
        ++cur->ticksPassedBy;
        printf("pid %d named %s priority %d ticks: %d ticksPassedBy %d ticksTotal: %d\n",cur->pid, cur->name, cur->priority, cur->ticks, cur->ticksPassedBy, cur->ticksTotal);
        //printf("its stack address: %d\n", cur->stack);
    }
    else
    {
        printf("Calling schedule!\n");
        programManager.schedule();
    }
}
```
* 调度函数`ProgramManager::schedule()`如下：
```c
void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // readyPrograms为空则说明只有1个进程，无需调度，直接返回
    if (readyPrograms.size() == 0)
    {
        interruptManager.setInterruptStatus(status);
        printf("---------------FINISH----------------");
        while(1){}
        return;
    }
    

    // running的变ready，且放到ready队列队尾
    if (running->status == ProgramStatus::RUNNING && running->ticksTotal)
    {
        running->status = ProgramStatus::READY;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::DEAD;
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }
    // 去就绪队列的第一个线程作为下一个执行的线程。
    // 就绪队列的第一个元素是ListItem *类型的，我们需要将其转换为PCB。
    // 注意到放入就绪队列readyPrograms的是每一个PCB的&tagInGeneralList，
    // 而tagInGeneralList在PCB中的偏移地址是固定的。
    // 也就是说，我们将item的值减去tagInGeneralList在PCB中的偏移地址就能够得到PCB的起始地址。
    // #define ListItem2PCB(ADDRESS, LIST_ITEM) ((PCB *)((int)(ADDRESS) - (int)&((PCB *)0)->LIST_ITEM))
    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    next->ticks = 3; //给下一个调度上来的进程分配时间片
    running = next;
    readyPrograms.pop_front();

    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}
```
* 轮转时间片设置为3，可以看到如下调度结果：
![alt text](img/image11.png)
![alt text](img/image12.png)
* 图中ticks是每次**时间片轮转的对应倒数**，ticksPassedBy是**当前线程已经获得的总时间数**，ticksTotal是**当前线程还需要的时间数**，可以看到正确地按照时间片轮转算法进行了调度

### 非抢占SJF
* 有2种实现思路：
* 1是在executeThread()创建线程的时候，就把线程按所需的运行总时间ticksTotal插入到readyPrograms队列中，这样调度时按照readyPrograms的顺序选front调度就好；
* 2是每次要调度新线程来执行时，都去比较哪个需要的执行时间是readyPrograms队列中最小的，再换上来执行
包括：主要工具安装使用过程及截图结果、程序过程中的操作步骤、测试数据、输入及输出说明、遇到的问题及解决情况、关键功能或操作的截图结果。不得抄袭，否则按作弊处理。
* 选择方法1实现：
* 更改线程创建函数`executeThread()`如下
```c
int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority, int time)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticksTotal = time;
    thread->ticks = thread->ticksTotal;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    // 初始化线程的栈。我们将栈放置在PCB中，而线程的栈是从PCB的顶部开始向下增长的，
    // 所以不会与位于PCB低地址的name和pid等变量冲突。线程栈的初始地址是PCB的起始地址加上PCB_SIZE
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    // 4个为0的值是要放到ebp，ebx，edi，esi中的
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    // thread->stack[4]是线程执行的函数的起始地址。
    thread->stack[4] = (int)function;
    // thread->stack[5]是线程的返回地址，所有的线程执行完毕后都会返回到这个地址。
    thread->stack[5] = (int)program_exit;
    // thread->stack[6]是线程的参数的地址。
    thread->stack[6] = (int)parameter;

    // 创建完线程的PCB后，我们将其放入到allPrograms和readyPrograms中，
    // 等待时钟中断来的时候，这个新创建的线程就可以被调度上处理器
    allPrograms.push_back(&(thread->tagInAllList));
    if(readyPrograms.empty()){
        readyPrograms.push_back(&(thread->tagInGeneralList));
    }else{
        ListItem *item = programManager.readyPrograms.front();
        PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
        if(thread->ticksTotal <= thisThread->ticksTotal){
            readyPrograms.push_front(&(thread->tagInGeneralList));
        }else{
            int pos = 1;
            ListItem *item = programManager.readyPrograms.at(pos);
            PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            while(thread->ticksTotal > thisThread->ticksTotal){
                if(pos == programManager.readyPrograms.size() - 1){
                    pos ++;
                    break;
                }
                pos ++;
                ListItem *item = programManager.readyPrograms.at(pos);
                PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            }
            programManager.readyPrograms.insert(pos, &(thread->tagInGeneralList));
        }
    }
    

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    printf("pid %d name %s priority %d totalTime %d CREATEDandREADY!\n",thread->pid,thread->name,thread->priority,thread->ticksTotal);
    //printf("Its addr: %d stack addr: %d\n",(int)thread, (int)thread->stack);

    return thread->pid;
}
```
* 得到如下结果：
![alt text](img/image13.png)
可以看到，线程确实是按照所需的总时间从小到大被调度上来运行的
### 优先级调度
* 稍微更改一下非抢占SJF的创建线程的函数，即用priority替换ticksTotal，就是优先级调度（SJF其实就是优先级调度的一种）
* executeThread函数如下部分修改：
```c
if(readyPrograms.empty()){
        readyPrograms.push_back(&(thread->tagInGeneralList));
    }else{
        ListItem *item = programManager.readyPrograms.front();
        PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
        if(thread->priority <= thisThread->priority){
            readyPrograms.push_front(&(thread->tagInGeneralList));
        }else{
            int pos = 1;
            ListItem *item = programManager.readyPrograms.at(pos);
            PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            while(thread->priority > thisThread->priority){
                if(pos == programManager.readyPrograms.size() - 1){
                    pos ++;
                    break;
                }
                pos ++;
                ListItem *item = programManager.readyPrograms.at(pos);
                PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            }
            programManager.readyPrograms.insert(pos, &(thread->tagInGeneralList));
        }
```
* 得到如下结果：
![alt text](img/image14.png)

### RR+SJF
* 把ROUND ROBIN和SJF结合，给每个线程分配3秒的时间片
* 那么schedule函数和executeThread函数稍微改动如下：
```c
int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority, int time)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticksTotal = time;
    thread->ticks = 3;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    // 初始化线程的栈。我们将栈放置在PCB中，而线程的栈是从PCB的顶部开始向下增长的，
    // 所以不会与位于PCB低地址的name和pid等变量冲突。线程栈的初始地址是PCB的起始地址加上PCB_SIZE
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    // 4个为0的值是要放到ebp，ebx，edi，esi中的
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    // thread->stack[4]是线程执行的函数的起始地址。
    thread->stack[4] = (int)function;
    // thread->stack[5]是线程的返回地址，所有的线程执行完毕后都会返回到这个地址。
    thread->stack[5] = (int)program_exit;
    // thread->stack[6]是线程的参数的地址。
    thread->stack[6] = (int)parameter;

    // 创建完线程的PCB后，我们将其放入到allPrograms和readyPrograms中，
    // 等待时钟中断来的时候，这个新创建的线程就可以被调度上处理器
    allPrograms.push_back(&(thread->tagInAllList));
    if(readyPrograms.empty()){
        readyPrograms.push_back(&(thread->tagInGeneralList));
    }else{
        ListItem *item = programManager.readyPrograms.front();
        PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
        if(thread->ticksTotal <= thisThread->ticksTotal){
            readyPrograms.push_front(&(thread->tagInGeneralList));
        }else{
            int pos = 1;
            ListItem *item = programManager.readyPrograms.at(pos);
            PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            while(thread->ticksTotal > thisThread->ticksTotal){
                if(pos == programManager.readyPrograms.size() - 1){
                    pos ++;
                    break;
                }
                pos ++;
                ListItem *item = programManager.readyPrograms.at(pos);
                PCB *thisThread = ListItem2PCB(item, tagInGeneralList);
            }
            programManager.readyPrograms.insert(pos, &(thread->tagInGeneralList));
        }
    }
    

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    printf("pid %d name %s priority %d totalTime %d CREATEDandREADY!\n",thread->pid,thread->name,thread->priority,thread->ticksTotal);
    //printf("Its addr: %d stack addr: %d\n",(int)thread, (int)thread->stack);

    return thread->pid;
}

void ProgramManager::schedule()
{
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // readyPrograms为空则说明只有1个进程
    if (readyPrograms.size() == 0 && running->ticksTotal == 0)
    {
        interruptManager.setInterruptStatus(status);
        printf("---------------FINISH----------------");
        while(1){}
        return;
    }
    

    // running的变ready，且放到ready队列队尾
    if (running->status == ProgramStatus::RUNNING && running->ticksTotal)
    {
        running->status = ProgramStatus::READY;
        readyPrograms.push_back(&(running->tagInGeneralList));
    }
    else if (running->status == ProgramStatus::RUNNING)
    {
        running->status = ProgramStatus::DEAD;
    }
    else if (running->status == ProgramStatus::DEAD)
    {
        releasePCB(running);
    }
    ListItem *item = readyPrograms.front();
    PCB *next = ListItem2PCB(item, tagInGeneralList);
    PCB *cur = running;
    next->status = ProgramStatus::RUNNING;
    next->ticks = 3; //给下一个调度上来的进程分配时间片
    running = next;
    readyPrograms.pop_front();

    asm_switch_thread(cur, next);

    interruptManager.setInterruptStatus(status);
}
```
* 可以看到结果如下：
![alt text](img/image15.png)
![alt text](img/image16.png)

## 实验总结

1. Assignment1让我明白了printf函数的简单实现机制 ，也学习了在C语言中可变参数函数机制是怎么样实现的；   
2. Assignment2让我在课堂理论学习的基础上，更具体地认识了PCB到底有哪些内容，且实际是怎样分配其相应空间的；    
3. Assignment3让我通过在GDB跟踪的过程中，不仅进一步熟悉了GDB调试跟踪的用法，还能在教程的指引下，去观察了esp这个存放PCB::stack的寄存器里的值的变化，从而在数值上认识到了线程是如何进行状态保存、切换，然后进行调度的；
4. 在Assignment4中，我通过自己实现一些简单的调度算法，熟悉了executeThread函数、schedule函数、c_time_interrupt_handler函数等，从而更加明白了线程是怎样被创建、线程调度、以及这个过程中是如何进行中断处理从而能调度的。更重要的是，通过自己修改executeThread函数、schedule函数、c_time_interrupt_handler函数，以及添加相关额外信息的输出，我熟悉了RR/FCFS/SJF等调度算法的具体操作，对调度算法的思想、工作流程更加清晰了。


每人必需写一段，文字不少于500字，可以写心得体会、问题讨论与思考、新的设想、感言总结或提出建议等等。不得抄袭，否则按作弊处理。

## 参考文献

Lab5实验教程：https://gitee.com/guifeng/sysu-2021-spring-operating-system/tree/main/lab5